<?php echo PHP_EOL;

# Copyright (C) 2020, camille.silverberg@gmail.com
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

define('ROOM_COUNT', 20);
define('PASSWORD', password_hash(
	'landscape gift overgrown blandness stood hamlet observing comfy',
	PASSWORD_ARGON2ID,
	[
		'memory_cost' => 2 **18,
		'time_cost' => 3,
		'threads' => 3,
	],
));

$rooms = [];
for ($index = 0; $index < ROOM_COUNT; $index += 1):
	$rooms[strval(1+$index)] = ['is_empty' => 0 === random_int(0, 1)];
endfor;

$room_is_empty = fn($room) => $room['is_empty'];
$not = fn($predicate) => fn($x) => FALSE === $predicate($x);

function stringify_array($things, $missing) {
	$count = count($things);
	if (0 === $count):
		return $missing;
	elseif (1 === $count):
		return $things[0];
	elseif (2 === $count):
		return $things[0] .' et '. $things[1];
	else:
		$things[$count-1] = 'et '. $things[$count-1];
		return implode(', ', $things);
	endif;
}

function authenticate() {
	echo 'Cette option necessite un mot de passe.' .PHP_EOL;
	$password = readline('Mot de passe: ');
	if (password_verify($password, PASSWORD)):
		echo 'Mot de passe accepte.' .PHP_EOL;
		return TRUE;
	else:
		echo 'Mot de passe refuse.' .PHP_EOL;
		return FALSE;
	endif;
}

$user_did_exit = FALSE;
while (FALSE === $user_did_exit):
	echo '-------------------------------MENU HOTEL DWWMPHP------------------------------------------------' .PHP_EOL;
	echo "\011". 'A ― Afficher l’etat de l’hôtel' .PHP_EOL;
	echo "\011". 'B ― Afficher le nombre de chambres reservees' .PHP_EOL;
	echo "\011". 'C ― Afficher le nombre de chambres libres' .PHP_EOL;
	echo "\011". 'D ― Afficher le numero de la première chambre vide' .PHP_EOL;
	echo "\011". 'E ― Afficher le numero de la dernière chambre vide' .PHP_EOL;
	echo "\011". 'F ― Reserver une chambre (Le programme doit reserver la première chambre vide)' .PHP_EOL;
	echo "\011". 'G ― Liberer une chambre (Le programme doit liberer la dernière chambre occupee)' .PHP_EOL;
	echo "\011". 'Q ― Quitter' .PHP_EOL;
	echo "\011". 'S ― Source et licence' .PHP_EOL;
	echo '---------------------------------------------------------------------------------------------------------------------' .PHP_EOL;
	
	$choice = readline('Votre choix : ');
	if ('Q' === $choice):
		echo 'byebye' .PHP_EOL;
		$user_did_exit = TRUE;
	elseif ('A' === $choice):
		$empty_rooms = array_keys(array_filter($rooms, $room_is_empty));
		echo 'Chambres libres: '. stringify_array($empty_rooms, 'aucune') .'.' .PHP_EOL;
		$full_rooms = array_keys(array_filter($rooms, $not($room_is_empty)));
		echo 'Chambres reservees: '. stringify_array($full_rooms, 'aucune') .'.' .PHP_EOL;
	elseif ('B' === $choice):
		$empty_rooms_count = count(array_filter($rooms, $room_is_empty));
		if (0 === $empty_rooms_count):
			echo 'Aucune chambre n’est libre.' .PHP_EOL;
		elseif (1 === $empty_rooms_count):
			echo 'Une seule chambre est libre.' .PHP_EOL;
		elseif (ROOM_COUNT === $empty_rooms_count):
			echo 'Toutes les chambres sont libre.' .PHP_EOL;
		else:
			echo 'Il y a '. strval($empty_rooms_count) .' chambres libre.' .PHP_EOL;
		endif;
	elseif ('C' === $choice):
		$full_rooms_count = count(array_filter($rooms, $not($room_is_empty)));
		if (0 === $full_rooms_count):
			echo 'Aucune chambre n’est prise.' .PHP_EOL;
		elseif (1 === $full_rooms_count):
			echo 'Une seule chambre est prise.' .PHP_EOL;
		elseif (ROOM_COUNT === $full_rooms_count):
			echo 'Toutes les chambres sont prise.' .PHP_EOL;
		else:
			echo 'Il y a '. strval($full_rooms_count) .' chambres prise.' .PHP_EOL;
		endif;
	elseif ('D' === $choice):
		$first_empty_room = array_key_first(array_filter($rooms, $room_is_empty));
		if (NULL === $first_empty_room):
			echo 'Toutes les chambres sont prise.' .PHP_EOL;
		else:
			echo 'La chambre '. $first_empty_room .' est la premiere chambre libre.' .PHP_EOL;
		endif;
	elseif ('E' === $choice):
		$last_empty_room = array_key_last(array_filter($rooms, $room_is_empty));
		if (NULL === $last_empty_room):
			echo 'Toutes les chambres sont prise.' .PHP_EOL;
		else:
			echo 'La chambre '. $last_empty_room .' est la derniere chambre libre.' .PHP_EOL;
		endif;
	elseif ('F' === $choice):
		if (authenticate()):
			$first_empty_room = array_key_first(array_filter($rooms, $room_is_empty));
			if (NULL === $first_empty_room):
				echo 'Aucune chambre n’a ete reservee, elles sont toutes prise.' .PHP_EOL;
			else:
				echo 'La chambre '. $first_empty_room .' a ete reservee.' .PHP_EOL;
				$rooms[$first_empty_room]['is_empty'] = FALSE;
			endif;
		endif;
	elseif ('G' === $choice):
		if (authenticate()):
			$last_full_room = array_key_last(array_filter($rooms, $not($room_is_empty)));
			if (NULL === $last_full_room):
				echo 'Aucune chambre n’a ete libree, elle sont toutes libre.' .PHP_EOL;
			else:
				echo 'La chambre '. $last_full_room .' a ete liberee.' .PHP_EOL;
				$rooms[$last_full_room]['is_empty'] = TRUE;
			endif;
		endif;
	elseif ('S' === $choice):
		echo 'Ce programme est distribue sous licence GNU AGPLv3.' .PHP_EOL;
		echo 'Son code source est disponible sur le lien suivant:' .PHP_EOL;
		echo '<https://bitbucket.org/DWWM_20044_J_pl/hotel/>' .PHP_EOL;
	else:
		echo 'choix non reconnu' .PHP_EOL;
	endif;
endwhile;